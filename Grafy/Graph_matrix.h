#pragma once
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <iomanip>
#include <functional>
#include <algorithm>
#define INF 0x3f3f3f3f

using namespace std;


#pragma region Struktury
template<typename Key>
struct Edge2 {
	Key index;
	int value;
	Edge2(Key we, int i) :index(we), value(i) {};
	Edge2() :value(0) {};
	Edge2<Key>& operator =(const Edge2<Key> & other) {if(this!=&other){ this->index = other.index; this->value = other.value; } return *this; };
};

struct Zbior {
	int size;
	int *rodzice, *rn;
	Zbior(int i) :size(i) { rodzice = new int[i+1]; rn = new int[i+1]; for (int j(0); j < i; j++) { rodzice[j] = j; rn[j] = 0; } };
	int znajdz(int i) { if (i != rodzice[i]) rodzice[i] = znajdz(rodzice[i]); return rodzice[i]; };
	void scal(int i, int y) { 
		i = znajdz(i); y = znajdz(y);
		if (rn[i] > rn[y]) rodzice[y] = i;
		else rodzice[i] = y;
		if (rn[i] == rn[y]) rn[y]++;
	};
};

#pragma endregion


template<typename Typ>
inline int znajdz_w_tab(Typ *tab, Typ w, int rozm) {
	for (int i(0); i < rozm; i++) {
		if (tab[i] == w)
			return i;
	}
	return -1;
}

template<typename Key>
inline bool Compare_index(pair<Key, pair<int, int>> const & p1, pair<Key, pair<int, int>> const & p2) { return p1.first < p2.first; }

template<typename Key>
inline bool Compare_index2(pair<Key, int> const & p1, pair<Key, int> const & p2) { return p1.first < p2.first; }

template<typename Typ, typename Key>
class Graph_matrix
{
	Typ *Vertices;
	Edge2<Key> **Matrix;
	int size, iter;
	bool *czy_zajete;
public:
	Graph_matrix(int i);
	Graph_matrix(Graph_matrix<Typ, Key> &G2);
	~Graph_matrix();
	void InsertVertex(Typ o);
	void InsertEdge(Typ v, Typ w, Key o);
	void RemoveVertex(Typ v);
	void RemoveEdge(Key e);
	Typ* endVertices(Key e);
	Typ opposite(Typ v, Key e);
	bool areAdjacent(Typ v, Typ w);
	void replace(Typ v, Typ x);
	void replaceEdge(Key e, Key x);
	void vertices();
	void edges();
	void edges_ind();
	int min_klucz(vector<Key> key, vector<bool> uzyte);
	vector<pair<pair<Typ, Typ>, Key>> Kruskal();
	vector<pair<pair<Typ, Typ>, Key>> Prim();
	void zapis_plik(string nazwa);
	void odczyt_plik(string nazwa);
};


template<typename Typ, typename Key>
Graph_matrix<Typ, Key>::Graph_matrix(int i){
	size = i; iter = 0;
	Vertices = new Typ[i];
	czy_zajete = new bool[i];
	Matrix = new Edge2<Key> *[i];
	for (int j(0); j < i; j++)
		Matrix[j] = new Edge2<Key>[i];
}

template<typename Typ, typename Key>
inline Graph_matrix<Typ, Key>::Graph_matrix(Graph_matrix<Typ, Key>& G2)
{
	this->size = G2.size; this->iter = G2.iter;

	Vertices = new Typ[size];
	czy_zajete = new bool[size];
	Matrix = new Edge2<Key> *[size];
	for (int j(0); j < size; j++)
		Matrix[j] = new Edge2<Key>[size];

	for (int i(0); i < size; i++) {
		czy_zajete[i] = G2.czy_zajete[i];
			Vertices[i] = G2.Vertices[i];
		for (int k(0); k < size; k++)
			Matrix[i][k] = G2.Matrix[i][k];
	}
}


template<typename Typ, typename Key>
Graph_matrix<Typ, Key>::~Graph_matrix()
{
	delete[] Vertices;
	for (int i(0); i < size; i++)
		delete[] Matrix[i];
	delete[] Matrix;
	delete[] czy_zajete;
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::InsertVertex(Typ o)
{
	bool uwaga(false);
	if (iter < size) {
		for (int i(0); i < size; i++)
			if (Vertices[i] == o)
				uwaga = true;
		if (uwaga == false) {
			czy_zajete[iter] = false;
			Vertices[iter++] = o;
		}
		else
			cout << "juz istnieje krawedz o takim oznaczeniu\n";
	}
	else
		cerr << "graf pe�en\n";
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::InsertEdge(Typ v, Typ w, Key o)
{
	int j(znajdz_w_tab<Typ>(Vertices, v, size)), k(znajdz_w_tab<Typ>(Vertices, w, size));
	if (j != (-1) && k != (-1) && Matrix[j][k].value == 0) {
		Edge2<Key> tmp(o, 1);
		Matrix[j][k] = tmp;
		Matrix[k][j] = tmp;
	}
	else if (Matrix[j][k].value == 1)
		cout << "juz istnieje polaczenie pomiedzy tymi wierzcholkami\n";
	else
		cerr << "Niepoprawne krawedzie\n";
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::RemoveVertex(Typ v)
{
	int j(znajdz_w_tab<Typ>(Vertices, v, size));
	if (j == (-1))
		cerr << "Brak takiej krawedzi\n";
	else {
		czy_zajete[--iter] = true;
		for (int i(j); i < (size-1); i++) {
			Vertices[i] = Vertices[i + 1];
			for (int k(0); k < (size); k++) {
				Matrix[i][k] = Matrix[i + 1][k];
				Matrix[size - 1][k] = Edge2<Key>();
			}
		}
		for (int i(0); i < size; i++) {
			for (int k(j); k < (size - 1); k++)
				Matrix[i][k] = Matrix[i][k + 1];
			Matrix[i][size - 1] = Edge2<Key>();
		}
	}
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::RemoveEdge(Key e)
{
	for (int i(0); i < size; i++) {
		for (int j(0); j < size; j++)
			if (Matrix[i][j].index == e) {
				Matrix[i][j] = Edge2<Key>();
				Matrix[j][i] = Edge2<Key>();
			}
	}
}

template<typename Typ, typename Key>
inline Typ * Graph_matrix<Typ, Key>::endVertices(Key e)
{
	Typ tmp[2];
	for(int i(0);i<size;i++)
		for (int j(0); j < size; j++)
			if(Matrix[i][j].index==e){
				tmp[0] = Vertices[i]; tmp[1] = Vertices[j];
			}
		
	return tmp;
}

template<typename Typ, typename Key>
inline Typ Graph_matrix<Typ, Key>::opposite(Typ v, Key e)
{
	for (int i(0); i<size; i++)
		for (int j(0); j < size; j++)
			if (Matrix[i][j].value==1 && Matrix[i][j].index == e) {
				if (Vertices[i] == v)
					return Vertices[j];
				else if (Vertices[j] == v)
					return Vertices[i];
				else cerr << "Brak krawedzi czy cos\n";
			}
	return Typ();
}

template<typename Typ, typename Key>
inline bool Graph_matrix<Typ, Key>::areAdjacent(Typ v, Typ w)
{
	int i = znajdz_w_tab(Vertices, v, size), j = znajdz_w_tab(Vertices, w, size);
	if (Matrix[i][j].value == 1)
		return true;
	return false;
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::replace(Typ v, Typ x)
{
	int i(znajdz_w_tab(Vertices, v, size));
	Vertices[i] = x;
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::replaceEdge(Key e, Key x)
{
	for(int i(0);i<size;i++)
		for(int j(0);j<size;j++)
			if (Matrix[i][j].index == e) {
				Matrix[i][j].index = x;
				return;
			}
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::vertices()
{
	for (int i(0); i < size; i++) {
		cout << "krawedz nr. " << i << " = ";
		if (!czy_zajete[i])
			cout << Vertices[i];
		else
			cout << "nic";
		cout<< endl;
	}
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::edges()
{
	cout << "   ";
	for (int i = 0; i < size; i++) cout << setw(3) << i;
	cout << endl << endl;
	for (int i = 0; i < size; i++)
	{
		cout << setw(3) << i;
		for (int j = 0; j < size; j++) cout << setw(3) << Matrix[i][j].value;
		cout << endl;
	}
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::edges_ind()
{
	for (int i(0); i < size; i++)
		for (int j(i); j < size;j++)
			if (Matrix[i][j].value == 1)
				cout << Matrix[i][j].index << " od wierzcholka: " << Vertices[i] << " do wierzcholka: " << Vertices[j] << endl;
}

template<typename Typ, typename Key>
inline int Graph_matrix<Typ, Key>::min_klucz(vector<Key> klucze, vector<bool> uzyte){//z przylegajacych wierzcholkow wybiera ten o najmniejszym kluczu
	int  min_index(0);
	Key min = INF;
	for (int v = 0; v < size; v++)
		if (uzyte[v] == false && klucze[v] < min)
			min = klucze[v], min_index = v;
	return min_index;
}

template<typename Typ, typename Key>
inline vector<pair<pair<Typ, Typ>, Key>> Graph_matrix<Typ, Key>::Kruskal()
{
	Key waga=Key();
	vector<pair<pair<Typ, Typ>, Key>> result;
	vector<pair<Key, pair<int, int>>> tmp;
	for (int i(0); i < size; i++) {
		for (int j(0); j < size; j++) {
			pair<Key, pair<int, int>> pom(Matrix[i][j].index, pair<int, int>(j,i));
			if(Matrix[i][j].value==1 && find(tmp.begin(), tmp.end(), pom) == tmp.end())
				tmp.push_back(pair<Key, pair<int, int>>(Matrix[i][j].index,pair<int,int>(i,j)));
		}
	}
	
	sort(tmp.begin(),tmp.end(), Compare_index<Key>);
	Zbior zbio(size);

	for (auto it = tmp.begin(); it != tmp.end(); it++) {
		int k = it->second.first;
		int h = it->second.second;
		int zb_k = zbio.znajdz(k);
		int zb_h = zbio.znajdz(h);

		if (zb_k != zb_h) {
			result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Vertices[k], Vertices[h]), it->first));
			waga += it->first;
			zbio.scal(zb_h, zb_k);
		}
	}
	result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Typ(), Typ()),waga));
	return result;
}

template<typename Typ, typename Key>
inline vector<pair<pair<Typ, Typ>, Key>> Graph_matrix<Typ, Key>::Prim(){
	vector<pair<pair<Typ, Typ>, Key>> result;
	Key waga = Key();
	vector<int> parent(size);
	vector<Key> klucze(size, INF);
	vector<bool> uzyte(size, false);

	klucze[0] = 0;
	parent[0] = -1;

	for (int i(0); i < size; i++) {
		int u = min_klucz(klucze, uzyte);
		uzyte[u] = true;
		for (int j(0); j < size; j++) {
			if (Matrix[u][j].value == 1 && uzyte[j] == false && Matrix[u][j].index < klucze[j]) {
				parent[j] = u, klucze[j] = Matrix[u][j].index;
			}
		}
	}

	for (int y(1); y < size; y++) {
		result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Vertices[parent[y]], Vertices[y]), Matrix[y][parent[y]].index));
		waga += klucze[y];
	}
	result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Typ(), Typ()), waga));
	return result;
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::zapis_plik(string nazwa){
	fstream plik;
	nazwa += ".txt";
	plik.open(nazwa, ios::out);
	string napis;
	if (plik.good()) {
		plik << "info dla uzytkownika: typ - " << typeid(Typ).name() << "  klucz - " << typeid(Key).name() << endl;
		plik << size << " " << iter << endl;

		for (int j(0); j < size; j++)
			plik << Vertices[j]<<" ";
		plik << endl;

		for(int i(0);i<size;i++)
			for (int k(i); k < size; k++) {
				if (Matrix[i][k].value == 1)
					plik << Matrix[i][k].index << " " << Vertices[i] << " " << Vertices[k] << endl;
			}
		plik.close();
	}
}

template<typename Typ, typename Key>
inline void Graph_matrix<Typ, Key>::odczyt_plik(string nazwa){
	nazwa += ".txt";
	int iter_f = 0, size_f = 0;
	fstream file(nazwa, ios::in);
	string tmp;
	Typ tmp2 = Typ(), tmp3 = Typ();
	Key edge = Key();
	vector<pair<Typ, Typ>> vec;

	if (file.good()) {
		getline(file, tmp);
		file >> size_f, file >> iter_f;
		Graph_matrix<Typ, Key> Graf(size);
		for (int j(0); j < iter_f; j++) {
			file >> tmp2;
			Graf.InsertVertex(Typ(tmp2));
		}
		InsertVertex(19);
		while (!file.eof()) {
			file >> edge, file >> tmp2, file >> tmp3;
			if (find(vec.begin(), vec.end(), pair<Typ, Typ>(tmp2, tmp3)) == vec.end()) {
				Graf.InsertEdge(tmp2, tmp3, edge);
				vec.push_back(pair<Typ, Typ>(tmp2, tmp3));
				vec.push_back(pair<Typ, Typ>(tmp3, tmp2));
			}
		}

		file.close();
		////przepisanie wartosci pomocniczego grafu do grafu this(do zrobienia lepiej)
		this->size = Graf.size; this->iter = Graf.iter;

		this->Vertices = new Typ[size];
		this->czy_zajete = new bool[size];
		this->Matrix = new Edge2<Key> *[size];
		for (int j(0); j < size; j++)
			this->Matrix[j] = new Edge2<Key>[size];

		for (int i(0); i < size; i++) {
			this->czy_zajete[i] = Graf.czy_zajete[i];
			this->Vertices[i] = Graf.Vertices[i];
			for (int k(0); k < size; k++)
				this->Matrix[i][k] = Graf.Matrix[i][k];
		}
		////
	}
	else
		cerr << "problem z otwarciem pliku\n";
}

