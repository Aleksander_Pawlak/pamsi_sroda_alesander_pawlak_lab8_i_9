#pragma once
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <fstream>
#include <algorithm>
#include <typeinfo>
#define INF 0x3f3f3f3f

using namespace std;

template<typename Key>
inline bool Compare_indexL(pair<Key, pair<int, int>> const & p1, pair<Key, pair<int, int>> const & p2) { return p1.first < p2.first; }


#pragma region Struktury
template<typename Key>
struct Edge {
	Key index;
	int destiny;
	Edge(Key in, int poz) :index(in), destiny(poz) {};
	bool operator ==(const Edge &A) { if (A.index==this->index) return true; return false; };
};

struct ZbiorL {
	int size;
	int *rodzice, *rn;
	ZbiorL(int i) :size(i) { rodzice = new int[i + 1]; rn = new int[i + 1]; for (int j(0); j < i; j++) { rodzice[j] = j; rn[j] = 0; } };
	int znajdz(int i) { if (i != rodzice[i]) rodzice[i] = znajdz(rodzice[i]); return rodzice[i]; };
	void scal(int i, int y) {
		i = znajdz(i); y = znajdz(y);
		if (rn[i] > rn[y]) rodzice[y] = i;
		else rodzice[i] = y;
		if (rn[i] == rn[y]) rn[y]++;
	};
};

#pragma endregion

template<typename Typ, typename Key>
class Graph_lista
{
	vector<Typ> vertex;
	vector<list<Edge<Key>>> LS;
	int i, size;
public:
	Graph_lista(int size);
	Graph_lista(Graph_lista<Typ, Key> &G2);
	~Graph_lista();
	void InsertVertex(Typ o);
	void InsertEdge(Typ v, Typ w, Key o);
	void RemoveVertex(Typ v);
	void RemoveEdge(Key e);
	Typ* endVertices(Key e);
	Typ opposite(Typ v, Key e);
	bool areAdjacent(Typ v, Typ w);
	void replace(Typ v, Typ x);
	void replaceEdge(Key e, Key x);
	void vertices();
	void edges();
	int min_klucz(vector<Key> key, vector<bool> uzyte);
	vector<pair<pair<Typ, Typ>, Key>> Kruskal();
	vector<pair<pair<Typ, Typ>, Key>> Prim();
	void zapis_plik(string nazwa);
	void odczyt_plik(string nazwa);
	Graph_lista<Typ, Key>& operator=(const Graph_lista<Typ, Key> & G2);
	bool czy_pelen() { if (i == size)return true; return false; }
};

template<typename Typ, typename Key>
Graph_lista<Typ,Key>::Graph_lista(int size)
{
	i = 0; this->size = size;
	vector<list<Edge<Key>>> tmp(size);
	LS = tmp;
}

template<typename Typ, typename Key>
inline Graph_lista<Typ, Key>::Graph_lista(Graph_lista<Typ, Key>& G2)
{
	i = G2.i; size = G2.size;
	vertex = G2.vertex; LS = G2.LS;
}

template<typename Typ, typename Key>
Graph_lista<Typ, Key>::~Graph_lista()
{
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::InsertVertex(Typ o)
{
	if (i < size && find(vertex.begin(),vertex.end(),o)==vertex.end()) {
		vertex.push_back(o);
		i++;
	}
	else if(i>=size){
		cout << "graf jes pe�ny\n";
	}
	else
		cout << o<<" juz jest taki wierzcholek, oznacz go inaczej\n";
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::InsertEdge(Typ v, Typ w, Key o)
{
	int uwaga(0);
	vector<Typ>::iterator ii = find(vertex.begin(), vertex.end(), v);
	if (ii != vertex.end()) {
		int i = distance(vertex.begin(), ii);
		vector<Typ>::iterator jj = find(vertex.begin(), vertex.end(), w);
		if (jj != vertex.end()) {
			int j = distance(vertex.begin(), jj);
			for (auto it = LS[i].begin(); it != LS[i].end(); ++it)
				if (it->destiny == j)
					uwaga = 1;
			if(uwaga==1)
				cout << "juz istnieje polaczenie miedzy tymi wierzcholkami\n";
			else {
					Edge<Key> tmp(o, j);
					LS[i].push_back(tmp);
					LS[j].push_back(Edge<Key>(o, i));
				}
		}
		else
			cout << "Brak odpowiedniego wierzcholka " << w << endl;
	}
	else
		cout << "Brak odpowiedniego wierzcholka " << v<<endl;
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::RemoveVertex(Typ v)
{
	vector<Typ>::iterator ii = find(vertex.begin(), vertex.end(), v);
	if (ii != vertex.end()) {
		int i = distance(vertex.begin(), ii);
		vertex.erase(vertex.begin() + i);
		LS[i].clear();
		list<Edge<Key>>::iterator it2;
		for (auto it = LS.begin(); it != LS.end(); ++it) {
			it2 = (*it).begin();
			while (it2 != (*it).end())
			{
				if ((*it2).destiny==i)
					it2 = (*it).erase(it2);
				else
					it2++;
			}
		}
	}
	else
		cout << "Nie ma takiego wierzcholka\n";
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::RemoveEdge(Key e)
{
	list<Edge<Key>>::iterator it2;
	for (auto it = LS.begin(); it != LS.end(); ++it) {
		it2 = (*it).begin();
		while (it2 != (*it).end())
		{
			if ((*it2).index == e)
				it2 = (*it).erase(it2);
			else
				it2++;
		}
	}
}

template<typename Typ, typename Key>
inline Typ * Graph_lista<Typ, Key>::endVertices(Key e)
{
	Typ *tmp = new Typ[2];
	int i, j;
	for (auto it = LS.begin(); it != LS.end(); ++it) {
		for (auto it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			if ((*it).index == e) {
				i = distance(LS.begin(), it);
				j = (*it).destiny;
			}
		}
	}
	tmp[0] = vertex[i];
	tmp[1] = vertex[j];
	return tmp;
}

template<typename Typ, typename Key>
inline Typ Graph_lista<Typ, Key>::opposite(Typ v, Key e)
{
	Typ tmp;
	vector<Typ>::iterator it = find(vertex.begin(), vertex.end(); v);
	if (it != vertex.end()) {
		int i = distance(vertex.begin(), it);
		int j = size + 1;
		for (auto it2 = LS[i].begin(); it2 != LS[i].end(); ++it2) {
			if ((*it).index == e)
				j = (*it).destiny;
		}
		if (j != size + 1)
			tmp = vertex[j];
		else cout << "brak odpowiedniej krawedzi\n";
	}
	else cout << "brak odpowiedniego wierzcholka\n";
	return tmp;
}

template<typename Typ, typename Key>
inline bool Graph_lista<Typ, Key>::areAdjacent(Typ v, Typ w)
{
	vector<Typ>::iterator it = find(vertex.begin(), vertex.end(); v);
	if (it != vertex.end()) {
		int i = distance(vertex.begin(), it);
		int j;
		for (auto it2 = LS[i]; it2 != LS[i].end(); ++it2) {
			j = (*it2).destiny;
			if (vertex[j] == w)
				return true;
		}
	}
	return false;
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::replace(Typ v, Typ x)
{
	vector<Typ>::iterator it = find(vertex.begin(), vertex.end(); v);
	if (it != vertex.end()) {
		*it = x;
	}
	else cout << "Brak odpowiedniego wierzcholka\n";
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::replaceEdge(Key e, Key x)
{
	list<Edge<Key>>::iterator it2;
	for (auto it = LS.begin(); it != LS.end(); ++it) {
		it2 = find((*it).begin(), (*it).end(), e);
		if (it2 != (*it).end())
			(*it2).index = x;
	}
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::vertices()
{
	for (auto it = vertex.begin(); it != vertex.end(); ++it) {
		cout << *it << endl;
	}
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::edges()
{
	for (auto it = LS.begin(); it != LS.end(); ++it) {
		for (auto it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			//cout << (*it2).index << " pomiedzy wierzcholkiem:  " << vertex[distance((*it).begin(),it2)] << " a wierzcholkiem:  " << vertex[(*it2).destiny] << endl;
			cout << it2->index << " pomiedzy wierzcholkiem:  " << vertex[distance(LS.begin(), it)] << " i wierzcholkiem " << vertex[it2->destiny] << endl;
		}
	}
}

template<typename Typ, typename Key>
inline int Graph_lista<Typ, Key>::min_klucz(vector<Key> key, vector<bool> uzyte)
{
	int  min_index(0);
	Key min = INF;
	for (int v = 0; v < size; v++)
		if (uzyte[v] == false && key[v] < min)
			min = key[v], min_index = v;

	return min_index;
}

template<typename Typ, typename Key>
inline vector<pair<pair<Typ, Typ>, Key>> Graph_lista<Typ, Key>::Kruskal()
{
	Key waga = Key();
	vector<pair<pair<Typ, Typ>, Key>> result;
	vector<pair<Key, pair<int, int>>> tmp;
	for (int i(0); i < size; i++) {
		for (auto it = LS[i].begin(); it != LS[i].end(); ++it) {
			pair<Key, pair<int, int>> pom(it->index, pair<int, int>(it->destiny, i));
			if(find(tmp.begin(), tmp.end(), pom) == tmp.end())
				tmp.push_back(pair<Key, pair<int, int>>(it->index, pair<int, int>(i, it->destiny)));
		}
	}

	sort(tmp.begin(), tmp.end(), Compare_indexL<Key>);
	ZbiorL zbio(size);

	for (auto it = tmp.begin(); it != tmp.end(); it++) {
		int k = it->second.first;
		int h = it->second.second;
		int zb_k = zbio.znajdz(k);
		int zb_h = zbio.znajdz(h);

		if (zb_k != zb_h) {
			result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(vertex[k], vertex[h]), it->first));
			waga += it->first;
			zbio.scal(zb_h, zb_k);
		}
	}
	result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Typ(), Typ()), waga));
	return result;
}

template<typename Typ>
int znajd_inde(list<Typ> A, int y) {//zwraca indeks elementu o szukanej wartosci
	for (auto it = A.begin(); it != A.end(); ++it) {
		if (it->destiny == y)
			return distance(A.begin(), it);
	}
}

template<typename Typ, typename Key>
inline vector<pair<pair<Typ, Typ>, Key>> Graph_lista<Typ, Key>::Prim()
{
	vector<pair<pair<Typ, Typ>, Key>> result;
	Key waga = Key();
	vector<int> parent(size);
	vector<Key> klucze(size, INF);
	vector<bool> uzyte(size, false);

	klucze[0] = 0;
	parent[0] = -1;

	for (int i(0); i < size; i++) {
		int u = min_klucz(klucze, uzyte);
		uzyte[u] = true;
		/*for (int j(0); j < size; j++) {
			if (Matrix[u][j].value == 1 && uzyte[j] == false && Matrix[u][j].index < klucze[j]) {
				parent[j] = u, klucze[j] = Matrix[u][j].index;
				cout << parent[j] << "   " << klucze[j] << endl;
			}
		}*/
		for(auto it=LS[u].begin();it!=LS[u].end();++it)
			if (uzyte[it->destiny] == false && it->index < klucze[it->destiny]) {
				parent[it->destiny] = u, klucze[it->destiny] = it->index;
			}
	}

	for (int y(1); y < size; y++) {
		unsigned N = znajd_inde(LS[y], parent[y]);//parent[y];
		auto it = LS[y].begin(); advance(it, N);
		result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(vertex[parent[y]], vertex[y]), it->index));
		waga += klucze[y];
	}
	result.push_back(pair<pair<Typ, Typ>, Key>(pair<Typ, Typ>(Typ(), Typ()), waga));
	return result;
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::zapis_plik(string nazwa)
{
	fstream plik;
	nazwa += ".txt";
	plik.open(nazwa, ios::out);
	string napis;
	if (plik.good()) {
		plik <<"info dla uzytkownika: typ - "<< typeid(Typ).name() <<"  klucz - "<< typeid(Key).name() <<endl;
		plik << size <<" "<<i<< endl;
		
		for (auto it = vertex.begin(); it != vertex.end(); ++it) {
			if (it != (vertex.end() - 1))
				plik << *it << " ";
			else
				plik << *it;
		}
		plik << endl;
		for (auto it = LS.begin(); it != LS.end(); ++it) 
			for (auto it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
				plik << it2->index << " " << vertex[distance(LS.begin(), it)] << " " << vertex[it2->destiny] << endl;
			}

		plik.close();
	}
}

template<typename Typ, typename Key>
inline void Graph_lista<Typ, Key>::odczyt_plik(string nazwa)
{
	nazwa += ".txt";

	int iter_f = 0, size_f = 0;
	fstream file(nazwa, ios::in);
	string tmp;
	Typ tmp2=Typ(),tmp3=Typ();
	Key edge=Key();
	vector<pair<Typ, Typ>> vec;

	if (file.good()) {
		getline(file, tmp);
		file >> size_f, file >> iter_f;
		Graph_lista<Typ, Key> Graf(size);
		for (int j(0); j < iter_f; j++) {
			file >> tmp2;
			Graf.InsertVertex(Typ(tmp2));
		}
		InsertVertex(19);
		while (!file.eof()) {
			file>>edge, file >> tmp2, file >> tmp3;
			if (find(vec.begin(), vec.end(), pair<Typ, Typ>(tmp2, tmp3)) == vec.end()) {
				Graf.InsertEdge(tmp2, tmp3, edge);
				vec.push_back(pair<Typ, Typ>(tmp2, tmp3));
				vec.push_back(pair<Typ, Typ>(tmp3, tmp2));
			}
		}

		file.close();
		////
		this->i = Graf.i; this->size = Graf.size;
		this->vertex = Graf.vertex; this->LS = Graf.LS;
		////
	}
	else
		cerr << "problem z otwarciem pliku\n";
}

template<typename Typ, typename Key>
inline Graph_lista<Typ, Key>& Graph_lista<Typ, Key>::operator=(const Graph_lista<Typ, Key>& G2)
{
	if (&G2 != this) {
		this->i = G2.i; this->size = G2.size;
		this->vertex = G2.vertex; this->LS = G2.LS;
	}
	return *this;
}