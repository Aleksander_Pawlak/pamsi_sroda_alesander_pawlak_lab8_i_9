#pragma once
#include <iostream>
#include "Graph_lista.h"
#include "Graph_matrix.h"
#include <ctime>
#include <string>
#include <Windows.h>
#include <fstream>
using namespace std;

Graph_lista<int, int> generuj_graf_l(int size, int gestosc, int zakres_wag) { //generuje losowy graf oparty na lstach o indeksach wierzcholkow i wagach krawedzi typu integer
	Graph_lista<int, int> Graf(size);
	int l_krawedzi = ((gestosc / 100.0)*size*(size - 1)) / 2;
	srand(time(NULL));

	for (int i(1); i <= size; i++)//dodaje wierzcholki o indeksach od 1 do size
		Graf.InsertVertex(i);

	cout << "wierzcholkowane\n";
	vector<pair<int, int>> s;
	int j(0);
	int v_od, v_do;


		while (j != l_krawedzi) {//dodaje krawedzie o losowych wagach z zakresu od 1 do zakres_wag do momentu, gdy jest ich tyle, aby gestosc byla rowna zadanej
		v_od = rand() % size + 1, v_do = rand() % size + 1;
		if (find(s.begin(), s.end(), pair<int, int>(v_od, v_do)) == s.end() && find(s.begin(), s.end(), pair<int, int>(v_do, v_od)) == s.end() && v_od != v_do)
			Graf.InsertEdge(v_od, v_do, rand() % zakres_wag + 1), s.push_back(pair<int, int>(v_od, v_do)), j++;
	}
		//Graf.InsertEdge(v_od, v_do, rand() % zakres_wag + 1);

	cout << "ddddd\n";
	return Graf;
}

Graph_matrix<int, int> generuj_graf_m(int size, int gestosc, int zakres_wag) { //generuje losowy graf oparty na lstach o indeksach wierzcholkow i wagach krawedzi typu integer
	Graph_matrix<int, int> Graf(size);
	int l_krawedzi = ((gestosc / 100.0)*size*(size - 1)) / 2;
	srand(time(NULL));

	for (int i(1); i <= size; i++)//dodaje wierzcholki o indeksach od 1 do size
		Graf.InsertVertex(i);

	vector<pair<int, int>> s;
	int j(0);
	int v_od, v_do;

	while (j != l_krawedzi) {//dodaje krawedzie o losowych wagach z zakresu od 1 do zakres_wag do momentu, gdy jest ich tyle, aby gestosc byla rowna zadanej
		v_od = rand() % size + 1, v_do = rand() % size + 1;
		if (find(s.begin(), s.end(), pair<int, int>(v_od, v_do)) == s.end() && find(s.begin(), s.end(), pair<int, int>(v_do, v_od)) == s.end() && v_od != v_do)
			Graf.InsertEdge(v_od, v_do, rand() % zakres_wag + 1), s.push_back(pair<int, int>(v_od, v_do)), j++;
	}

	return Graf;
}

void testy_na_100_l() {
	int size(0), gestosc(0), zakres(0), co(0);
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	//double elapsedTime;
	cout << "Przetestujemy sobie 100 grafow\n";
	// get ticks per second
	QueryPerformanceFrequency(&frequency);
	double wyniki[100];

	cout << "Podaj rozmiar grafow\n"; cin >> size;
	cout << "podaj gestosc grafow(w procentach)\n"; cin >> gestosc;
	cout << "podaj gorny zakres wag krawedzi\n"; cin >> zakres;
	cout << "Testujemy 1 - Kruskala, 2 - Prima\n";
	do {
		cin >> co;
		if (co != 1 && co != 2)
			cout << "zle\n";
	} while (co != 1 && co != 2);
	cout << "zaczynam algorytmy\n";
	for (int i(0); i < 100; i++) {
		Graph_lista<int, int> Graf(generuj_graf_l(size, gestosc, zakres));
		if (co == 1) {
			QueryPerformanceCounter(&t1);
			vector<pair<pair<int, int>, int>> wynik = Graf.Kruskal();
			QueryPerformanceCounter(&t2);
		}
		else {
			QueryPerformanceCounter(&t1);
			vector<pair<pair<int, int>, int>> wynik = Graf.Prim();
			QueryPerformanceCounter(&t2);
		}
		wyniki[i]= (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		}

	cout << "przepisuje\n";
	fstream file;
	file.open("testy.txt", std::ios::app | std::ios::ate);
	if (file.good()) {
		for (int j(0); j < 100; j++)
			file << wyniki[j] << ", ";
		file << endl;
		file.close();
	}
}

void testy_na_100_m() {
	int size(0), gestosc(0), zakres(0), co(0);
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
									//double elapsedTime;
	cout << "Przetestujemy sobie 100 grafow\n";
	// get ticks per second
	QueryPerformanceFrequency(&frequency);
	double wyniki[100];

	cout << "Podaj rozmiar grafow\n"; cin >> size;
	cout << "podaj gestosc grafow(w procentach)\n"; cin >> gestosc;
	cout << "podaj gorny zakres wag krawedzi\n"; cin >> zakres;
	cout << "Testujemy 1 - Kruskala, 2 - Prima\n";
	do {
		cin >> co;
		if (co != 1 && co != 2)
			cout << "zle\n";
	} while (co != 1 && co != 2);

	for (int i(0); i < 100; i++) {
		Graph_matrix<int, int> Graf(generuj_graf_m(size, gestosc, zakres));
		if (co == 1) {
			QueryPerformanceCounter(&t1);
			vector<pair<pair<int, int>, int>> wynik = Graf.Kruskal();
			QueryPerformanceCounter(&t2);
		}
		else {
			QueryPerformanceCounter(&t1);
			vector<pair<pair<int, int>, int>> wynik = Graf.Prim();
			QueryPerformanceCounter(&t2);
		}
		wyniki[i] = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	}


	fstream file;
	file.open("testy.txt", std::ios::app | std::ios::ate);
	if (file.good()) {
		for (int j(0); j < 100; j++)
			file << wyniki[j] << ", ";
		file << endl;
		file.close();
	}
}

template<typename Typ, typename Key>
void podmenu_l() {
	int wyb(0);
	Typ tmp(0), tmp2(0);
	Key tmp3(0);
	string nazwa;
	cout << "Podaj rozmiar grafu\n";
	cin >> wyb;
	Graph_lista<Typ, Key> Graf(wyb);
	wyb = 0;
	do {
		cout << "wybierz operacje:\n1.dodaj wierzcholek\n2. dodaj krawedz\n3. usun wierzcholek\n4. usun krawedz\n5. wyswietl wierzcholki\n6.wyswietl krawedzie\n";
		cout << "7. przetestuj algorytm kruskala\n8. przetestuj algorytm prima\n9.Zapis do pliku\n10. odczyt z pliku(nadpiszuje dotychczasowe wartosci w grafie)\n0. koniec\n";
		cout << "(UWAGA! algorytmy minimalnego drzewa rozpinajacego bazuja indeksach krawedzi o wybieranym typie)\n";
		cin >> wyb;
		switch (wyb) {
		case 1: cout << "podaj wartosc\n"; cin >> tmp; Graf.InsertVertex(tmp);  break;
		case 2: cout << "od jakiego wierzcholka: \n"; cin >> tmp; cout << "do jakiego: "; cin >> tmp2; cout << "podaj indeks:\n"; cin >> tmp3; Graf.InsertEdge(tmp, tmp2, tmp3); break;
		case 3: cout << "ktory\n"; cin >> tmp; Graf.RemoveVertex(tmp); break;
		case 4: cout << "podaj indeks\n"; cin >> tmp3; Graf.RemoveEdge(tmp3); break;
		case 5: Graf.vertices(); break;
		case 6: Graf.edges(); break;
		case 7: {	vector<pair<pair<Typ, Typ>, Key>> wynik = Graf.Kruskal();
			vector<pair<pair<Typ, Typ>, Key>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: "<<it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 8: {	vector<pair<pair<Typ, Typ>, Key>> wynik = Graf.Prim();
			vector<pair<pair<Typ, Typ>, Key>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 9: cout << "podaj nazwe pliku (np. file)\n"; cin >> nazwa; Graf.zapis_plik(nazwa); break;
		case 10: cout << "podaj nazwe pliku (np. file)\n"; cin >> nazwa; Graf.odczyt_plik(nazwa); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}

template<typename Typ>
void wybor_edge_l() {
	int wyb(0);

	do {
		cout << "wybierz typ krawedzi:\n1.int\n2.float\n3.char\n";
		cout << "(UWAGA! algorytmy minimalnego drzewa rozpinajacego bazuja indeksach krawedzi o wybieranym typie)\n";
		cin >> wyb;
		switch (wyb) {
		case 1: podmenu_l<Typ,int>(); break;
		case 2: podmenu_l<Typ, float>(); break;
		case 3: podmenu_l<Typ, char>(); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}

void wybierz_typ_l() {
	int wyb(0);
	
	do {
		cout << "wybierz typ wierzcholka:\n1.int\n2.float\n3.char\n";
		cin >> wyb;
		switch (wyb) {
		case 1: wybor_edge_l<int>(); break;
		case 2: wybor_edge_l<float>(); break;
		case 3: wybor_edge_l<char>(); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}


template<typename Typ, typename Key>
void podmenu_m() {
	int wyb(0);
	Typ tmp(0), tmp2(0);
	Key tmp3(0);
	string nazwa;
	cout << "Podaj rozmiar grafu\n";
	cin >> wyb;
	Graph_matrix<Typ, Key> Graf(wyb);
	wyb = 0;
	do {
		cout << "wybierz operacje:\n1.dodaj wierzcholek\n2. dodaj krawedz\n3. usun wierzcholek\n4. usun krawedz\n5. wyswietl wierzcholki\n6.wyswietl krawedzie\n";
		cout << "7. wyswietl krawedzie po ludzku\n8. przetestuj algorytm kruskala\n9. przetestuj algorytm prima\n10.Zapis do pliku\n11. odczyt z pliku(nadpiszuje dotychczasowe wartosci w grafie)\n0. koniec\n";
		cout << "(UWAGA! algorytmy minimalnego drzewa rozpinajacego bazuja indeksach krawedzi o wybieranym typie)\n";
		cin >> wyb;
		switch (wyb) {
		case 1: cout << "podaj wartosc\n"; cin >> tmp; Graf.InsertVertex(tmp);  break;
		case 2: cout << "od jakiego wierzcholka: \n"; cin >> tmp; cout << "do jakiego: "; cin >> tmp2; cout << "podaj indeks:\n"; cin >> tmp3; Graf.InsertEdge(tmp, tmp2, tmp3); break;
		case 3: cout << "ktory\n"; cin >> tmp; Graf.RemoveVertex(tmp); break;
		case 4: cout << "podaj indeks\n"; cin >> tmp3; Graf.RemoveEdge(tmp3); break;
		case 5: Graf.vertices(); break;
		case 6: Graf.edges(); break;
		case 7: Graf.edges_ind(); break;
		case 8: {	vector<pair<pair<Typ, Typ>, Key>> wynik = Graf.Kruskal();
			vector<pair<pair<Typ, Typ>, Key>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 9: {	vector<pair<pair<Typ, Typ>, Key>> wynik = Graf.Prim();
			vector<pair<pair<Typ, Typ>, Key>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 10: cout << "podaj nazwe pliku (np. file)\n"; cin >> nazwa; Graf.zapis_plik(nazwa); break;
		case 11: cout << "podaj nazwe pliku (np. file)\n"; cin >> nazwa; Graf.odczyt_plik(nazwa); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}

template<typename Typ>
void wybor_edge_m() {
	int wyb(0);

	do {
		cout << "wybierz typ krawedzi:\n1.int\n2.float\n3.char\n";
		cout << "(UWAGA! algorytmy minimalnego drzewa rozpinajacego bazuja indeksach krawedzi o wybieranym typie)\n";
		cin >> wyb;
		switch (wyb) {
		case 1: podmenu_m<Typ, int>(); break;
		case 2: podmenu_m<Typ, float>(); break;
		case 3: podmenu_m<Typ,char>(); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}

void wybierz_typ_m() {
	int wyb(0);

	do {
		cout << "wybierz typ wierzcholka:\n1.int\n2.float\n3.char\n";
		cin >> wyb;
		switch (wyb) {
		case 1: wybor_edge_m<int>(); break;
		case 2: wybor_edge_m<float>(); break;
		case 3: wybor_edge_m<char>(); break;
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}


void sprawdz_los_l(){
	int size, gestosc, zakres,wyb(0);
	cout << "podaj rozmiar grafu\n";
	cin >> size;
	cout << "podaj gestosc(w procentach)\n";
	cin >> gestosc;
	cout << "podaj gorna granice wielkosci wag\n";
	cin >> zakres;
	Graph_lista<int, int> Graf(generuj_graf_l(size, gestosc, zakres));
	
	do {
		cout << "co zrobic?\n1.wyswietl wierzcholki\n2. wyswietl krawedzie\n3.wyswietl wynik z kruskala\n4.wyswietl wynik z prima\n0. wyjdz\n";
		cin >> wyb;
		switch (wyb) {
		case 1: Graf.vertices(); break;
		case 2: Graf.edges(); break;
		case 3: {	vector<pair<pair<int, int>, int>> wynik = Graf.Kruskal();
			vector<pair<pair<int, int>, int>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 4: {	vector<pair<pair<int, int>, int>> wynik = Graf.Prim();
			vector<pair<pair<int, int>, int>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (size != 0);
}

void sprawdz_los_m() {
	int size, gestosc, zakres;
	cout << "podaj rozmiar grafu\n";
	cin >> size;
	cout << "podaj gestosc(w procentach)\n";
	cin >> gestosc;
	cout << "podaj gorna granice wielkosci wag\n";
	cin >> zakres;
	Graph_matrix<int, int> Graf(generuj_graf_m(size, gestosc, zakres));

	do {
		cout << "co zrobic?\n1.wyswietl wierzcholki\n2. wyswietl krawedzie\n3.wyswietl wynik z kruskala\n4.wyswietl wynik z prima\n0. wyjdz\n";
		cin >> size;
		switch (size) {
		case 1: Graf.vertices(); break;
		case 2: Graf.edges(); break;
		case 3: {	vector<pair<pair<int, int>, int>> wynik = Graf.Kruskal();
			vector<pair<pair<int, int>, int>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		case 4: {	vector<pair<pair<int, int>, int>> wynik = Graf.Prim();
			vector<pair<pair<int, int>, int>>::iterator it;
			for (it = wynik.begin(); it != wynik.end(); ++it) {
				if (it != (wynik.end() - 1))
					cout << "od: " << it->first.first << "  do:  " << it->first.second << "  waga:  " << it->second << endl;
				else
					cout << "suma wag:  " << it->second << endl;
			}
			break;
		}
		default: cout << "zly wybor\n"; break;
		}
	} while (size != 0);
}

void menu_glow(){
	int wyb(0);

	do {
		cout << "wybierz co chcesz zrobic:\n1. Sprawdzanie operacji na pojedynczym grafie\n2.Testy 100 grafow i zapis pomiarow czasu do pliku\n";
		cout<<"3. Przetestuj algorytmy kruskala i prima dla losowego grafu(krawedzie typu int, wagi typu int)\n0.wyjdz\n";
		cin >> wyb;
		switch (wyb) {
		case 1: {
			cout << "ktory graf chcesz testowac?\n1.Macierzowy\n2.oparty na liscie\n0. Wstecz\n";
			cin >> wyb;
			switch (wyb) {
			case 1: wybierz_typ_m(); break;
			case 2: wybierz_typ_l(); break;
			case 0: break;
			default: cout << "cos nieteges\n"; break;
			}
			break;
		}
		case 2: {
			cout << "ktory graf chcesz testowac?\n1.Macierzowy\n2.oparty na liscie\n0. Wstecz\n";
			cin >> wyb;
			switch (wyb) {
			case 1: testy_na_100_m(); break;
			case 2: testy_na_100_l(); break;
			case 0: break;
			default: cout << "cos nieteges\n"; break;
			}
			break;
		}
		case 3: {cout << "1 - Graf na liscie | 2 - Graf na macierzy\n 0 - wyjdz\n"; cin >> wyb; if (wyb == 1)sprawdz_los_l(); else if (wyb == 2)sprawdz_los_m(); break; }
		case 0: break;
		default: cout << "niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
}